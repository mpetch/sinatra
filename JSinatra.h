/*
 * While developing an OS,
 * forget all the #include <stdlib>,<iostream> thingies...
 * Say bye to memory management, gui, exceptions...
 * And precious heap...
 *
 * But, you are in Ring0.
 * Ring0        = Highest Privelege available on CPU.
 * That means...
 * You have control of everyhing on computer.
 * You have the ultimate power.
 * You can use all of the memory
 * = No more Memory access violations!
 * But be careful!
 * You can destroy all the hardware...
 * When you close the computer,
 * everyhing on the RAM dies.
 * But, some data maybe not died.
 * or RAM still contains rubbish,
 * or Assembly instructions may damage
 * your computer (formatting harddisk,
 *                resetting clocks,
 *                corrupting BIOS [settings]...)
 *
 * Skipping the narration and starting descripting...
 *
 * We are using 'multiple definition guard'
 * to block multiple definition, that causes
 * OS to not compile...
 *
 * We are also defined kernel functions here.
 * I used 'Jl' prefix to distinguish
 * kernel functions from other Sinatra functions
 * (as Windows uses 'Nt', 'Zw', 'Ob' prefixes),
 * and 'J' for constants since you can not use
 * 'classes' in C (but in C++, we can not use it
 * because pretty heavy for kernel development).
 *
 * JlVgaColor::JWhiteText is used as colorizing byte
 * in JlClearScreen and JlPrintF.
 * It makes the text black backgrounded (0x0)
 * and white colorized (0x7).
 */

#ifndef __SINATRA_H__
#define __SINATRA_H__
        /*
         * vga colors enumerations
         * to colorize text output
         */
#define JWhiteText 0x07
        
        void JlClearScreen();
        unsigned int JlPrintF(char *message, unsigned int line);
        //void JlUpdateCursor(int row, int col);

        void JlMain();
#endif

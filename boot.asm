; We have to use 32 bit (PMode)
; for kernel.
; That means...
; saying bye to interrupts...
; We used cli since we can not
; use them.
; And hlt for stopping at end.
; For not executing garbage instructions.

[BITS 32]
[global start]
[extern _JlMain]

start:
        cli
        call _JlMain
        hlt

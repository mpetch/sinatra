#include "JSinatra.h"

void JlClearScreen() // clear entire screen
{
        char *vidmem    = (char *) 0xb8000;
        unsigned int i  = 0;

        /*
         * vidmem is the pointer to video memory,
         * the segment used for text input i/o.
         * i is the offset.
         */
        
        while ( i < ( 80 * 25 * 2 ) )
        {
                /*
                 * 80 is the console width.
                 * 25 is the height.
                 * Also you have to multiply that with 2
                 * because 2nd byte used for colorizing.
                 */
                 vidmem[i]      = ' ';
                 i += 1;
                 // 1st byte    = Character.
                 vidmem[i]      = JWhiteText;
                 i += 1;
                 // 2nd byte    = Colorizing.

                 // And we went to other character.
        }
}

unsigned int JlPrintF(char *message, unsigned int line) {
        char *vidmem    = (char *) 0xb8000;
        unsigned int i  = 0;

        /*
         * vidmem is the pointer to video memory,
         * the segment used for text input i/o.
         * i is the offset.
         * message is the pointer to string to write
         * whose null terminated.
         */

         i              = line * 80 * 2;

         /*
          * line is the desired line number.
          * To jump the desired line,
          * we must multiply it with console width
          * (in this case it's 80).
          * And also multiply multiplication with 2
          * since 2nd byte is used for colorizing.
          */

          while ( *message  !=  0 ) {
                  /*
                   * Since we are using 'pointers',
                   * we should check byte by byte.
                   * We stop iterating when we hit an 'null'
                   * since C(++) uses null terminator for
                   * terminating strings.
                   */

                   if (*message == '\n')
                   {
                           /*
                            * Handle new line character.
                            * Without this, '\n' would look
                            * like some VGA specific character.
                            * 
                            */
                            line        += 1;
                            i           = (line * 80 * 2);
                            *message    += 1;

                           /*
                            * we can also use pointers like indice
                            * registers (offset).
                            * Currently, we are using it ('message')
                            * as string indice.
                            */
                   }
                   else
                   {
                           /*
                            * It is ordinary character.
                            * So, write it to video segment
                            * with 'i' offset.
                            * Since 2nd byte used for colorizing,
                            * also write on it.
                            */
                            vidmem[i]   = *message;
                            /*
                             * message indice points to character
                             * to write (next).
                             * 
                             * 1st byte = Character
                             * 2nd byte = Colorizing
                             */
                            *message    += 1;
                            i           += 1;
                            vidmem[i]   = JWhiteText;
                            i           += 1;
                            // And we went to next character...
                   }
          }
          return(1);
}

void JlMain()
{
        /*
         * initialize the Sinatra (OS).
         * clear the screen,
         * and write the message that indicates
         * Sinatra loaded correctly.
         */
        JlClearScreen();
        JlPrintF("Sinatra v0 Virgin/Kernel Mode\n", 0);
}
